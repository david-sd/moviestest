import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { MoviesComponent } from './movies/movies.component';
import { SeriesComponent } from './series/series.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { CardComponent } from './card/card.component';

import { GeneralService } from './general.service';
import { MoviesService } from './movies/movies.service';
import { SeriesService } from './series/series.service';


@NgModule({
  declarations: [
    AppComponent,
    MoviesComponent,
    SeriesComponent,
    FavoritesComponent,
    CardComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [GeneralService, MoviesService, SeriesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
