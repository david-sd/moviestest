import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { GeneralService } from '../general.service';
import { MoviesService } from '../movies/movies.service';
import { SeriesService } from '../series/series.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() data;
  @Output() trailerEvent = new EventEmitter();

  constructor(private generalSrv: GeneralService, private moviesSrv: MoviesService, private seriesSrv: SeriesService) { }

  ngOnInit() {
  }

  dateObject(date_string) {
    return new Date(date_string);
  }

  genreNames(genreIds) {
    let genres = [];
    if (this.data.release_date) {
      genres = JSON.parse(sessionStorage.getItem('movies_genre_list'));
    } else {
      genres = JSON.parse(sessionStorage.getItem('series_genre_list'));
    }
    let genreText = '';
    for (let id of genreIds) {
      let temp_genre = genres.find(x => (x.id == id));
      if (temp_genre && temp_genre.name) {
        genreText += ', ' + temp_genre.name;
      }
    }
    return genreText.substr(2);
  }

  findTrailer() {
    if (this.data.release_date) {
      this.moviesSrv.getVideos(this.data.id).subscribe(
        response => {
          let trailer = response.results.find(x => (x.type === "Trailer"));
          // console.log("https://www.youtube.com/watch?v=" + trailer.key);
          // if (trailer.key) {
            this.trailerEvent.emit(trailer.key);
          // }
        },
        error => {
          console.error(error);
        }
      );
    } else {
      this.seriesSrv.getVideos(this.data.id).subscribe(
        response => {
          console.log(response);
          let trailer = response.results.find(x => (x.type === "Trailer"));
          // console.log("https://www.youtube.com/watch?v=" + trailer.key);
          // if (trailer.key) {
            this.trailerEvent.emit(trailer.key);
          // }
        },
        error => {
          console.error(error);
        }
      );
    }
  }

  inFavs() {
    return this.generalSrv.inFavorites(this.data);
  }

  addToFavs() {
    this.generalSrv.addToFavorites(this.data);
  }

  removeFromFavs() {
    this.generalSrv.removeFromFavorites(this.data);
  }

}
