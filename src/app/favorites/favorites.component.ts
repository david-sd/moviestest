import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { GeneralService } from '../general.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss'],
  providers: [GeneralService]
})
export class FavoritesComponent implements OnInit {

  years:Array<any> = [];
  genres:Array<any> = [];
  list:Array<any> = [];
  pagination:any = {};
  pages:Array<any> = [];
  targetPage:number = 1;
  modal:any = {};
  searchText:string = "";
  yearFilter:string = "";
  genreFilter:number = 0;

  constructor(public sanitizer: DomSanitizer, private generalSrv: GeneralService) {
    let date = new Date();
    for (var i = date.getFullYear(); i >= (date.getFullYear() - 100); i--) {
      this.years.push(i);
    }
  }

  ngOnInit() {
    this.list = this.generalSrv.getFavorites();
  }

  fetchFavorites(options:any = {}) {
    if (this.yearFilter !== "") {
      console.log(this.list);
      this.list = this.list.filter(x => ((x.release_date && x.release_date.substr(0, 4) == this.yearFilter) || (x.first_air_date && x.first_air_date.substr(0, 4) == this.yearFilter)));
    } else {
      this.list = this.generalSrv.getFavorites();
    }
    if (this.searchText.length > 0) {
      this.list = this.list.filter(x => ((x.title && x.title.toLowerCase().includes(this.searchText.toLowerCase())) || (x.name && x.name.toLowerCase().includes(this.searchText.toLowerCase()))));
    }
  }

  onSearchChange() { 
    this.fetchFavorites();
  }

  onYearChange() { 
    this.fetchFavorites();
  }

  onGenreChange() { 
    this.fetchFavorites();
  }

  showTrailer(key) {
    if (key) this.modal.trailer_url = this.sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/" + key + "?autoplay=1");

    this.modal.active = true;
  }

  hideTrailer() {
    this.modal = {};
  }

  setPagination() {
    if (this.pagination.total_pages <= 8) {
      this.pagination.start_page = 1;
      this.pagination.end_page = this.pagination.total_pages;
    } else {
      if (this.pagination.page <= 4) {
        this.pagination.start_page = 1;
        this.pagination.end_page = 8;
      } else if ((this.pagination.page + 2) >= this.pagination.total_pages) {
        this.pagination.start_page = this.pagination.total_pages - 7;
        this.pagination.end_page = this.pagination.total_pages;
      } else {
        this.pagination.start_page = this.pagination.page - 3;
        this.pagination.end_page = this.pagination.page + 4;
      }
    }

    this.pagination.list = [];
    console.log(this.pagination.start_page, this.pagination.end_page);
    for (var i = this.pagination.start_page; i <= this.pagination.end_page; i++) {
      console.log("page" + i);
      this.pagination.list.push(i);
    }
  }

  goPage(page) {
    if ((page >= 1) && (page <= this.pagination.total_pages)) {
      this.targetPage = page;
      this.fetchFavorites({page: this.targetPage});
    }
  }

  emptyList() {
    if (this.list.length == 0) {
      return true;
    } else {
      return false;
    }
  }

  inFavs(item) {
    let value = this.generalSrv.inFavorites(item);
    if (value) {
      this.fetchFavorites();
    }
    return value;
  }

}
