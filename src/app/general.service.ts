import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { SERVER_URL, API_KEY } from './constants';

@Injectable()
export class GeneralService {

  private headers;

  constructor(private http: Http) {
    this.headers = {
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    };
  }

  get(key) {
    if (sessionStorage.getItem("app_data")) {
      let data = JSON.parse(sessionStorage.getItem("app_data"));
      if (data[key]) {
        return data[key];
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  set(key, value) {
    if (sessionStorage.getItem("app_data")) {
      let data = JSON.parse(sessionStorage.getItem("app_data"));
      data[key] = value;
      sessionStorage.setItem("app_data", JSON.stringify(data));
    }
  }

  setData(data) {
    sessionStorage.setItem("app_data", JSON.stringify(data));
  }

  getMovieGenres(): Observable<any> {
    let URL = SERVER_URL + 'genre/movie/list' + API_KEY;
    return this.http
      .get(URL, this.headers)
      .map(res => res.json());
  }

  getSeriesGenres(): Observable<any> {
    let URL = SERVER_URL + 'genre/tv/list' + API_KEY;
    return this.http
      .get(URL, this.headers)
      .map(res => res.json());
  }

  getFavorites() {
    return (localStorage.getItem('favorites_list')) ? JSON.parse(localStorage.getItem('favorites_list')) : [];
  }

  setFavorites(list: Array<any>) {
    localStorage.setItem('favorites_list', JSON.stringify(list));
  }

  inFavorites(item) {
    let favList = this.getFavorites();
    return favList.find(x => (x.id == item.id));
  }

  addToFavorites(item) {
    if (!this.inFavorites(item)) {
      let favList = this.getFavorites();
      favList.push(item);
      this.setFavorites(favList);
    }
  }

  removeFromFavorites(item) {
    if (this.inFavorites(item)) {
      let favList = this.getFavorites();
      favList = favList.filter(x => (x.id !== item.id));
      this.setFavorites(favList);
    }
  }

  getMovies(): Observable<any> {
    let URL = SERVER_URL + 'discover/movie' + API_KEY;
    return this.http
      .get(URL, this.headers)
      .map(res => res.json());
  }
}
