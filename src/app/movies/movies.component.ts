import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { MoviesService } from './movies.service';
import { GeneralService } from '../general.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss'],
  providers: [GeneralService, MoviesService]
})
export class MoviesComponent implements OnInit {

  years:Array<any> = [];
  genres:Array<any> = [];
  list:Array<any> = [];
  pagination:any = {};
  pages:Array<any> = [];
  targetPage:number = 1;
  modal:any = {};
  searchText:string = "";
  yearFilter:string = "";
  genreFilter:number = 0;

  constructor(public sanitizer: DomSanitizer, private generalSrv: GeneralService, private moviesSrv: MoviesService) {
    let date = new Date();
    for (var i = date.getFullYear(); i >= (date.getFullYear() - 100); i--) {
      this.years.push(i);
    }
  }

  ngOnInit() {
    if (!sessionStorage.getItem('movies_genre_list')) {
      this.generalSrv.getMovieGenres().subscribe(
        response => {
          console.log("Genres", response);
          this.genres = response.genres;
          sessionStorage.setItem('movies_genre_list', JSON.stringify(response.genres));
          this.fetchMovies();
        },
        error => {
          console.error(error);
        }
      );
    } else {
      this.genres = JSON.parse(sessionStorage.getItem('movies_genre_list'));
      console.log(this.genres);
      this.fetchMovies();
    }
  }

  fetchMovies(options:any = {}) {
    if (this.yearFilter !== "") {
      options.year = this.yearFilter;
    }
    if (this.genreFilter !== 0) {
      this.searchText = "";
      options.with_genres = this.genreFilter;
    }
    if (this.searchText.length == 0) {
      this.moviesSrv.getMovies(options).subscribe(
        response => {
          console.log("List", response);
          this.list = response.results;
          this.pagination.page = response.page;
          // this.pagination.total_pages = response.total_pages;
          this.pagination.total_pages = 1000;
          this.setPagination();

          window.scroll(0,0);
        },
        error => {
          console.error(error);
        }
      );
    } else {
      this.searchMovies();
    }
  }

  searchMovies(options:any = {}) {
    options.query = this.searchText;
    if (this.yearFilter !== "") {
      options.year = this.yearFilter;
    }
    this.moviesSrv.searchMovies(options).subscribe(
      response => {
        console.log("List", response);
        this.list = response.results;
        this.pagination.page = response.page;
        // this.pagination.total_pages = response.total_pages;
        if (this.pagination.total_pages > 1000) {
          this.pagination.total_pages = 1000;
        } else {
          this.pagination.total_pages = response.total_pages;
        }
        this.setPagination();

        window.scroll(0,0);
      },
      error => {
        console.error(error);
      }
    );
  }

  onSearchChange() { 
    if (this.searchText.length >= 3) {
      this.genreFilter = 0;
      this.searchMovies();
    } else {
      this.fetchMovies();
    }
  }

  onYearChange() { 
    this.fetchMovies();
  }

  onGenreChange() { 
    this.fetchMovies();
  }

  showTrailer(key) {
    if (key) this.modal.trailer_url = this.sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/" + key + "?autoplay=1");

    this.modal.active = true;
  }

  hideTrailer() {
    this.modal = {};
  }

  setPagination() {
    if (this.pagination.total_pages <= 8) {
      this.pagination.start_page = 1;
      this.pagination.end_page = this.pagination.total_pages;
    } else {
      if (this.pagination.page <= 4) {
        this.pagination.start_page = 1;
        this.pagination.end_page = 8;
      } else if ((this.pagination.page + 2) >= this.pagination.total_pages) {
        this.pagination.start_page = this.pagination.total_pages - 7;
        this.pagination.end_page = this.pagination.total_pages;
      } else {
        this.pagination.start_page = this.pagination.page - 3;
        this.pagination.end_page = this.pagination.page + 4;
      }
    }

    this.pagination.list = [];
    console.log(this.pagination.start_page, this.pagination.end_page);
    for (var i = this.pagination.start_page; i <= this.pagination.end_page; i++) {
      console.log("page" + i);
      this.pagination.list.push(i);
    }
  }

  goPage(page) {
    if ((page >= 1) && (page <= this.pagination.total_pages)) {
      this.targetPage = page;
      this.fetchMovies({page: this.targetPage});
    }
  }

  emptyList() {
    if (this.list.length == 0) {
      return true;
    } else {
      return false;
    }
  }

}
