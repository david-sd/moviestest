import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { SERVER_URL, API_KEY } from '../constants';

@Injectable()
export class SeriesService {

  private headers;

  constructor(private http: Http) {
    this.headers = {
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    };
  }

  getSeries(options:any = {}): Observable<any> {
    let optStr = "";
    let keys = Object.keys(options);
    for (let key of keys) {
        optStr += "&" + key + "=" + options[key];
    }
    let URL = SERVER_URL + 'discover/tv' + API_KEY + optStr;
    return this.http
      .get(URL, this.headers)
      .map(res => res.json());
  }

  searchSeries(options:any = {}): Observable<any> {
    let optStr = "";
    let keys = Object.keys(options);
    for (let key of keys) {
        optStr += "&" + key + "=" + options[key];
    }
    let URL = SERVER_URL + 'search/tv' + API_KEY + optStr;
    return this.http
      .get(URL, this.headers)
      .map(res => res.json());
  }

  getVideos(id): Observable<any> {
    let URL = SERVER_URL + 'tv/' + id +'/videos' + API_KEY;
    return this.http
      .get(URL, this.headers)
      .map(res => res.json());
  }
}
